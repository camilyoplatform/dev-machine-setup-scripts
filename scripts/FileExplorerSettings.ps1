#--- Configuring Windows properties ---
#--- Windows Features ---
# Show file extensions
Set-WindowsExplorerOptions -EnableShowFileExtensions

# Show hidden files, Show protected OS files, Show file extensions
# Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -EnableShowProtectedOSFiles -EnableShowFileExtensions


#--- File Explorer Settings ---
#opens PC to This PC, not quick access
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name LaunchTo -Value 1
#taskbar where window is open for multi-monitor
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name MMTaskbarMode -Value 2

