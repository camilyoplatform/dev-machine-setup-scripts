# Enable optional features for IIS

Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-ApplicationDevelopment
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-ApplicationInit
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-ASPNET45
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-BasicAuthentication
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-CommonHttpFeatures
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-DefaultDocument
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-DirectoryBrowsing
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-HealthAndDiagnostics
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-HttpCompressionDynamic
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-HttpCompressionStatic
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-HttpErrors
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-HttpLogging
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-HttpRedirect
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-IIS6ManagementCompatibility
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-ISAPIExtensions
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-ISAPIFilter
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-LegacyScripts
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-ManagementConsole
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-Metabase
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-NetFxExtensibility45
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-Performance
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-RequestFiltering
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-RequestMonitor
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-Security
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-StaticContent
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-WebServer
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-WebServerManagementTools
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-WebServerRole
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-WindowsAuthentication
Enable-WindowsOptionalFeature -Online -All -FeatureName IIS-WMICompatibility
Enable-WindowsOptionalFeature -Online -All -FeatureName NetFx3
Enable-WindowsOptionalFeature -Online -All -FeatureName NetFx4-AdvSrvs
Enable-WindowsOptionalFeature -Online -All -FeatureName NetFx4Extended-ASPNET45
Enable-WindowsOptionalFeature -Online -All -FeatureName WCF-HTTP-Activation45
Enable-WindowsOptionalFeature -Online -All -FeatureName WCF-Services45
Enable-WindowsOptionalFeature -Online -All -FeatureName WCF-TCP-PortSharing45

#--- Install URL Rewrite (required for xmobile app) ---
choco install --cacheLocation="c:\temp" -y urlrewrite