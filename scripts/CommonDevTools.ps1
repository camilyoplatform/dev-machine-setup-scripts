
# tools we expect devs across many scenarios will want
choco install --cacheLocation="c:\temp" -y powershell-core --install-arguments='"ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1"' # pwsh.exe
choco install --cacheLocation="c:\temp" -y vscode
choco install --cacheLocation="c:\temp" -y notepadplusplus
choco install --cacheLocation="c:\temp" -y git --package-parameters="'/GitAndUnixToolsOnPath /WindowsTerminal'"

choco install --cacheLocation="c:\temp" -y 7zip.install
choco install --cacheLocation="c:\temp" -y sysinternals
choco install --cacheLocation="c:\temp" -y winrar
choco install --cacheLocation="c:\temp" -y heidisql
choco install --cacheLocation="c:\temp" -y git-fork
choco install --cacheLocation="c:\temp" -y postman
choco install --cacheLocation="c:\temp" -y ditto
choco install --cacheLocation="c:\temp" -y lockhunter
choco install --cacheLocation="c:\temp" -y tortoisegit
choco install --cacheLocation="c:\temp" -y winmerge
choco install --cacheLocation="c:\temp" -y ngrok
choco install --cacheLocation="c:\temp" -y azure-cli
choco install --cacheLocation="c:\temp" -y microsoft-windows-terminal

choco install --cacheLocation="c:\temp" -y microsoft-teams

# use https://www.microsoft.com/en-il/p/microsoft-remote-desktop/9wzdncrfj3ps instead
# choco install --cacheLocation="c:\temp" -y rdcman