choco install --cacheLocation="c:\temp" -y paint.net
choco install --cacheLocation="c:\temp" -y teracopy

# Powershell bookmarks
# https://www.powershellmagazine.com/2016/05/06/powershell-location-bookmark-for-easy-and-faster-navigation/
Install-Module -Name PSBookmark