#--- .NET 4.7.2 and 4.8 ---
choco install --cacheLocation="c:\temp" -y netfx-4.7.2-devpack
choco install --cacheLocation="c:\temp" -y netfx-4.8-devpack

choco install --cacheLocation="c:\temp" -y dotnetcore-sdk # .NET Core 3.1 SDK
choco install --cacheLocation="c:\temp" -y dotnet # .NET 6 SDK
choco install --cacheLocation="c:\temp" -y dotnetcore-windowshosting # .NET Core 3.1 hosting pack
choco install --cacheLocation="c:\temp" -y dotnet-windowshosting # .NET 6 hosting pack

#--- Visual Studio 2017 Professional - removed since we are moving to JetBrains Rider
# For enterprise use package visualstudio2017enterprise
# choco install --cacheLocation="c:\temp" -y visualstudio2017professional --package-parameters="'--add Microsoft.VisualStudio.Component.Git --add Microsoft.Net.Component.4.7.2.SDK --add Microsoft.Net.Component.4.7.2.TargetingPack --add Microsoft.Net.Component.4.6.2.SDK --add Microsoft.Net.Component.4.6.2.TargetingPack --add Microsoft.Net.ComponentGroup.4.7.DeveloperTools --add Microsoft.VisualStudio.Component.LinqToSql --add Microsoft.VisualStudio.Workload.Azure --add Microsoft.VisualStudio.Workload.ManagedDesktop --add Microsoft.VisualStudio.Workload.NetCoreTools --add Microsoft.VisualStudio.Workload.NetWeb --includeRecommended'"
# RefreshEnv #refreshing env due to Git install

# Build tools (Optional)
# choco install --cacheLocation="c:\temp" -y visualstudio2017buildtools

#--- JetBrains tools ---
choco install --cacheLocation="c:\temp" -y jetbrains-rider
