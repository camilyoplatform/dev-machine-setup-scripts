# About this project

The goal of this repository is to automate initial setup of Camilyo web developer Windows machine.

These scripts leverage two popular open source projects.

- Boxstarter [boxstarter.org](http://boxstarter.org)
- Chocolatey [chocolatey.org](http://chocolatey.org)

Boxstarter is a wrapper for Chocolatey and includes features like managing reboots for you.

## Project structure

The script code is organized in a hierarchy

**Recipes**
A recipe is the script you run. It calls multiple helper scripts. These currently live in the root of the project (there's only one recipe at the moment, dev_camilyo.ps1)

**Helper Scripts**: A helper script performs setup routines that may be useful by many recipes. Recipes call helper scripts (you don't run helper scripts directly). The helper scripts live in the **scripts** folder

## You may want to customize the scripts

These scripts should cover a lot of what you need but will not likely match your personal preferences exactly. In this case please fork the project and change the scripts however you desire. We really appreciate PR's back to this project if you have recommended changes.

_Note: The one-click links use the following format. When working out of a different Fork or Branch you'll want to update the links as follows:_

`http://boxstarter.org/package/url?https://bitbucket.org/camilyoplatform/dev-machine-setup-scripts/raw/YOUR_BRANCH/RECIPE_NAME.ps1 `

For more info on testing your changes take a look at the [contribution guidelines](CONTRIBUTING.md).

## How to run the scripts

### Option 1 - Run in Powershell

Instructions copied from https://boxstarter.org/Learn/WebLauncher.

Run as Administrator:

```powershell
# Install Boxstarter
. { iwr -useb https://boxstarter.org/bootstrapper.ps1 } | iex; Get-Boxstarter -Force

# run
Install-BoxstarterPackage -PackageName https://bitbucket.org/camilyoplatform/dev-machine-setup-scripts/raw/master/dev_camilyo.ps1
```

### Option 2 - Use the web launcher (unsupported)

**The web launcher won't work with Chromium-based browsers, see Known Issues below.**

To run a recipe script, click a link in the table below from your target machine. This will download the Boxstarter one-click application, and prompt you for Boxstarter to run with Administrator privileges (which it needs to do its job). Clicking yes in this dialog will cause the recipe to begin. You can then leave the job unattended and come back when it's finished.

| Click link to run                                                                                                                               | Description                      |
| ----------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| [Camilyo Web Dev](http://boxstarter.org/package/url?https://bitbucket.org/camilyoplatform/dev-machine-setup-scripts/raw/master/dev_camilyo.ps1) | Standard Camilyo web dev machine |

**Notes:**

1. If you are using WSL there's a followup step we recommend after running the setup script. When the script finishes you will only have a root user with a blank password. You should manually create a non-root user via `$ sudo adduser [USERNAME] sudo`
   with a non-blank password. Use this user going forward. For more info on WSL please refer to the [documentation](https://docs.microsoft.com/en-us/windows/wsl/about).
2. If you're a Node.js contributor working on Node.js core, please see the [Node.js Bootstrapping Guide](https://github.com/nodejs/node/tree/master/tools/bootstrap) or [click here to run](http://boxstarter.org/package/nr/url?https://raw.githubusercontent.com/nodejs/node/master/tools/bootstrap/windows_boxstarter).

## Customizations

In order to run a modified version of these scripts, you can create a branch, modify scripts, modify the web launcher URL, commit and push.

1. You can install Visual Studio Enterprise instead of Professional if you have a license (Normally team leads). Simply edit [this script](scripts/DotNetTools.ps1).

## Known issues

- The Boxstarter ClickOnce installer (web launcher) will only work on legacy Edge (pre-Chromium version). This issue is being tracked [here](https://github.com/chocolatey/boxstarter/issues/345).
- Reboot is not always logging you back in to resume the script. This is being tracked [here](https://github.com/chocolatey/boxstarter/issues/318). The workaround is to login manually and the script will continue running.
- There have been reports of Windows 1803 not successfully launching Boxstarter via the web launcher. See this issue for details: https://github.com/chocolatey/boxstarter/issues/301

## Setting up a VM

Windows 10 VM setup instructions

1. Use Hyper-V's [Quick Create](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/quick-create-virtual-machine) to set up a VM
2. Once signed in to your VM, visit this project in a web browser and click one of the script links in the Readme

# Legal

Please read before using scripts.

#### Using our scripts downloads third party software

When you use our sample scripts, these will direct to Chocolately to install the packages.
By using Chocolatey to install a package, you are accepting the license for the application, executable(s), or other artifacts delivered to your machine as a result of a Chocolatey install. This acceptance occurs whether you know the license terms or not. Read and understand the license terms of any package you plan to install prior to installation through Chocolatey. If you do not want to accept the license of a package you are installing, you need to uninstall the package and any artifacts that end up on your machine as a result of the install.

#### Our samples are provided AS-IS without any warranties of any kind

Chocolately has implemented security safeguards in their process to help protect the community from malicious or pirated software, but any use of our scripts is at your own risk. Please read the Chocolately's legal terms of use and the Boxstarter project license as well as how the community repository for Chocolatey.org is maintained.

Our project is subject to the MIT License and we make no warranties, express or implied of any kind. In no event is Microsoft or contributing copyright holders be liable for any claim, damages or other liability arising from out of or in connection with the use of the project software or the use of other dealings in the project software.

# Contributing

Do you want to contribute? We would love your help. Here are our <a href="CONTRIBUTING.md">contribution guidelines</a>.
